# Parameter Variabel diketahui
skala = 1500000
jarakpeta = 8
kecepatan = 60
awaljam = 07.30

# Mencari Jarak Sebenarnya
jaraksebenarnya = int((jarakpeta * skala)/100000)

# Mencari waktu tempuh
waktu = int(jaraksebenarnya/kecepatan)

# Menambahkan waktu tiba
pukul = "{:.2f}".format(awaljam + waktu)

# Menampilkan Hasil
print("Jarak Sebenarnya Kota A ke Kota B adalah", jaraksebenarnya, "km")
print("Waktu Kota A ke Kota B adalah", waktu, "jam")
print("Pukul", pukul, "Kereta Api tiba di Kota B")


