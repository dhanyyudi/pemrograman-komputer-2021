a = 3 # Andi
b = 4 #Banu
c = 5 #Candra


# Jumlah Umur Banu dan Candra (b+c) = 36
bc = 36

# Umur Banu
ub = int((b*(bc))/(b+c))

# Umur Candra
uc = int((c*(bc))/(b+c))

# Umur Andi
ua = int((uc/c)*3)

# Jumlah Umur Ketiganya
ut = int(ub + uc + ua)

# Hasil
print("Menghitung Perbandingan Umur dan Jumlah Umur")
print("Umur Andi =", ua, "tahun")
print("Umur Banu =", ub, "tahun")
print("Umur Candra =", uc, "tahun")
print("Jumlah Umur Ketiganya =", ut, "tahun")

