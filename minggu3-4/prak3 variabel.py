# Skrip program penggunaan variabel dan penugasannya
# serta mengatur tampilannya, disimpan dengan nama file
# prak3 variabel.py

# operator penugasan
x, y, z = 100, 25.45, -28
x /= 5
y //= 3
z %= -5

#penulisan bilangan riil
bill1 = 75.123456789012345
bill2 = int(bill1)
bill3 = str(bill1)
bill4 = "{:E}".format(bill1)
bill5 = "{:.7f}".format(bill1)
bill6 = "{:.3f}".format(-bill1)
bill7 = "{:.3E}".format(bill1*100)
bill8 = "{:.3E}".format(bill1/1000)
bill9 = "{:,}".format(bill1*10000000)
bill10 = "{:.2%}".format(bill1/100)

#konversi nilai integer bill ket tipe biner tanpa awalan "0b"
bil = "{:b}".format(bill2)

#tampilkan bilangan
print("-----------Hasil dari Nomor 1-----------")
print("----------------------------------------")
print("-----------Operator Penugasan-----------")
print("Nilai akhir dari X =", x)
print("Nilai akhir dari Y =", y)
print("Nilai akhir dari Z =", z)
print("----------------------------------------")
print("-----------Penulisan Bilangan Riil-----------")
print("Nilai dari bill1 =", bill1)
print("Nilai dari bill2 =", bill2)
print("Nilai dari bill3 =", bill3)
print("Nilai dari bill4 =", bill4)
print("Nilai dari bill5 =", bill5)
print("Nilai dari bill6 =", bill6)
print("Nilai dari bill7 =", bill7)
print("Nilai dari bill8 =", bill8)
print("Nilai dari bill9 =", bill9)
print("Nilai dari bill10 =", bill10)
print("Nilai dari bil =", bil)
