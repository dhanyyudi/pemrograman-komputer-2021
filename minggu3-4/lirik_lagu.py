# variabel
judul = "angel"
penyanyi = "denny caknan ft. cak percil"
paragraf1 = "ketika semuanya terasa begitu abot. kucoba untuk tetap rapopo. Disaat cinta ini terasa angel. Angel."
paragraf2 = "trenso kuwi ra koyo instagram. seng diklik langsung oleh ati. duh gusti nopo kulo di prank. ra kuat ati iki pas dee medot janji."
paragraf3 = "ayumu tenanan ora editan. seng marai aku kedanan. pancen salahku dewe. ra ono seng ngongkon. abot sanggane aku angel move on."
paragraf4 = "ketika semuanya terasa begitu abot. kucoba untuk tetap rapopo. disaat cinta ini terasa angel. angel"
paragraf5 = "kukatakan dalam hati yo uwes. menurutmu aku kudu piye. apakah aku harus mengikutimu. yo aku mengkis mengkis."

print('{:^150}'.format(judul.upper()))
print('----------------------------------------------------------------------------------------------------------------------------------------------------------------------------')
print('{:>160}'.format(penyanyi.title()))
print('----------------------------------------------------------------------------------------------------------------------------------------------------------------------------')

print('{:<10}'.format(paragraf1.title()))
print('{:>200}'.format(paragraf2.title()))
print('{:^150}'.format(paragraf3.title()))
print('{:<10}'.format(paragraf4.title()))
print('{:>200}'.format(paragraf5.title()))