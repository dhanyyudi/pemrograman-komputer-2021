# input tinggi dan jari-jari
a = input("Masukkan nilai jari-jari: ")
b = input("Masukkan tinggi tabung: ")

# nilai jari-jari dan tinggi serta phi
r = int(a)
t = int(b)

phi = int(22/7)

# rumus luas permukaan tabung
lt = (2*phi*r*r)+(2*phi*r*t)

#rumus volume tabung
vt = phi*r*r*t

print("Menghitung Luas Permukaan dan Volume Tabung")
print("-------------------------------------------")
print("Hasil Luas Permukaan Tabung =", lt)
print("Hasil Volume Tabung =", vt)