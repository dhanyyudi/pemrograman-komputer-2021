artis = "isyana sarasvati"
lagu = "keep being you"
rilis = 2014
copy = 15000000
album = "explore!"
pesan = 'Lagu “{lagu}” yang dinyanyikan “{artis}” dari album “{album}” yang dirilis pada tahun {rilis} telah terjual sebanyak {copy} kopi'

print("-------------------A Cara default {}.format---------------------------")
print('Lagu “{}” yang dinyanyikan “{}” dari album “{}” yang dirilis pada tahun {} telah terjual sebanyak {:,} kopi'.format(lagu.title(), artis.title(), album.title(), rilis, copy))

print("-------------------B Menggunakan Index {index}.format---------------------------")
print('Lagu “{0}” yang dinyanyikan “{1}” dari album “{2}” yang dirilis pada tahun {3} telah terjual sebanyak {4:,} kopi'.format(lagu.title(), artis.title(), album.title(), rilis, copy))

print("-------------------C Menuliskan Variabel Penampung {variable}.format---------------------------")
print(pesan.format(lagu = "Keep Being You", artis = "Isyana Sarasvati", album = "Explore!", rilis = 2014 , copy = "15,000,000" ))

print("-------------------Menggunakan titik---------------------------")
print(pesan.format(lagu = "Keep Being You", artis = "Isyana Sarasvati", album = "Explore!", rilis = 2014 , copy = "15.000.000" ))