'''         Contoh skrip program pemakaian nested-for        '''

print("Contoh kalang bersarang, i kalang luar dan j kalang dalam")
print("-" * 32)

for i in range(1, 4):
    print("Untuk i =", i, " --> kalang luar")
    for j in range(1, 3):
        print("  maka j =", " --> kalang dalam")
    print("-" * 32)
