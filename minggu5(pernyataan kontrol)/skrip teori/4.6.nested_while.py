'''         Contoh skrip program pemakaian nested-while        '''

print("Contoh kalang bersarang, i kalang luar dan j kalang dalam")
print("-" * 32)

i = 1
while i < 4:
    print("Untuk i =", i, " --> kalang luar")
    j = 1
    while j < 3:
        print(" maka j =", j, " --> kalang dalam")
        j += 1
    print("-" * 32)
    i += 1
