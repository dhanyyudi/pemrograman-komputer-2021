''' Contoh skrip program penulisan if... else... dalam satu baris '''

bil1 = int(input("Masukkan bilangan bulat: "))
bil2 = bil1

#Penggunaan if... else... tidak dalam satu baris
if bil1 > 100:
    bil1 *=2
else:
    bil1 /= 5
print("Nilai bill =", bil1)

#Penggunaan if... else... dalam satu baris
bil2 = bil2 * 2 if bil2 > 100 else bil2 / 5
print("Nilai bil2 =", bil2)

#Penggunaan multi else dalam satu baris
hasil = print("bill < bil2") if bil1 < bil2 else print("bil1 > bil2")\
    if bil1 > bil2 else print("bil1 = bil2")