'''         Contoh skrip program efek continue pada pernyataan while        '''

i = 0

while i < 10:
    i += 1
    if i % 2 == 1:
        continue
    print("i =", i, "--> Selamat Belajar Python")