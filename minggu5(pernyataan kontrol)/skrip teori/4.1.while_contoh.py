''' Contoh pemakaian while untuk menampilkan tulisan '''
'''  Selamat Belajar Python lebih dari satu kali     '''
''' dan menampilkan bilangan ganjil dari 30 sampai 0 '''

i = 1

while i<= 5:
    print("i=", i, "---> Selamat Belajar Python")
    i += 1

print("\nBilangan ganjil dari 30 sampai 0 adalah:")
ganjil = 29

while ganjil >= 1:
    print(ganjil, end = ' ')
    ganjil -= 2

print('\nProgram selesai')