''' Contoh skrip program penggunaan pernyataan if... elif... '''
'''        untuk mengkonversi nilai angka ke huruf           '''

print("Konversi Nilai Angka ke Nilai Huruf")
print(">= 85 : A    >= 70 - < 85 : B    >= 55 - < 70 : C")
print(">=40 - < 55 : D    < 40 : E/n")

angka = float(input("Masukkan nilai angka yang akan dikonversi : "))

if angka >=85:
    huruf = 'A'
elif angka >=70:
    huruf = 'B'
elif angka >=55:
    huruf = 'C'
elif angka >=40:
    huruf = 'D'
else:
    huruf = 'E'

print("\nHasil konversi nilai angka", angka,\
      "dalam huruf adalah =", huruf)