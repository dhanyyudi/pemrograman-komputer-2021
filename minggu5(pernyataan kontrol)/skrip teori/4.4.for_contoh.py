'''         Contoh pemakaian pengulangan for        '''
'''  untuk menghitung rata-rata dari sejumlah data    '''

print("Program menghitung nilai rata-rata\n")
banyak_data = int(input("Banyaknya data = "))

jumlah = 0

for pencacah in range(1, banyak_data + 1):
    print("Untuk data ke", pencacah, end = ' ')
    nilai = int(input("Masukkan nilainya = "))

    jumlah += nilai

print("\nNilai rata-ratanya =", jumlah / banyak_data)