'''         Contoh pemakaian pengulangan while        '''
'''  untuk menghitung rata-rata dari sejumlah data    '''

print("Program menghitung nilai rata-rata\n")
banyak_data = int(input("Banyaknya data = "))

pencacah = 1
jumlah = 0

while (pencacah <= banyak_data):
    print("Untuk data ke", pencacah, end = ' ')
    nilai = int(input("Masukkan nilainya = "))

    jumlah += nilai
    pencacah += 1

print("\nNilai rata-ratanya =", jumlah / banyak_data)