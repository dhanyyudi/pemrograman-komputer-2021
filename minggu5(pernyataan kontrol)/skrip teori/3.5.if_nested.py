''' Contoh skrip program penggunaan if bersarang '''
'''        dengan 2 tingkat if                   '''

angka = int(input("Masukkan suatu bilangan bulat: "))

if angka % 2 == 0:
    if angka % 3 == 0:
        print('\nBilangan yang anda masukkan', angka,\
              'merupakan bilangan kelipatan 2 dan 3')

print("\nProgram selesai")