''' Contoh skrip program penggunaan pernyataan if... else... '''
'''        dengan modifikasi dari program if sederhana       '''

bil = int(input("Masukkan suatu bilangan bulat: "))

if bil > 20:
    print("Bilangan yang anda masukkan =", bil)
    print("Berarti lebih besar dari 20")
else:
    print("Bilangan yang anda masukkan =", bil)
    print("Berarti lebih kecil dari 20")

print("\nProgram selesai")