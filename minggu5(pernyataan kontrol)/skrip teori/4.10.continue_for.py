'''         Contoh skrip program efek continue pada pernyataan for        '''

for i in range(0, 10):
    if i % 2 == 1:
        continue
    print("i =", i, "--> Selamat Belajar Python")