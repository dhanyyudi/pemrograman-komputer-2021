''' Contoh pemakaian operator logika pada pernyataan if '''

print("Masukkan 2 buah bilangan positif atau negatif\n")

x = float(input("Nilai x = "))
y = float(input("Nilai y = "))

if x >= 0 and y >= 0:
    print("\nKedua bilangan yang anda masukkan semuanya positif")
elif x < 0 and y < 0:
    print("\nKedua bilangan yang anda masukkan semuanya negatif")
else:
    print("\nSalah satu dari 2 bilangan tersebut ada yang negatif")

print("\nProgram selesai")