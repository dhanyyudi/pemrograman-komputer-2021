''' Contoh Skrip program penggunaan pernyataan if sederhana '''

bil = int(input("Masukkan suatu bilangan bulat: "))

if bil > 20:
    print("Bilangan yang anda masukkan =", bil)
    print("Berarti lebih besar dari 20")

print("\nProgram selesai")