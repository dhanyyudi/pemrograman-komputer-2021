''' Contoh skrip program penggunaan if bersarang '''
'''  dengan 3 tingkat if sekaligus bercabang     '''

usia = int(input("Masukkan usia anda : "))
qty = int(input("Masukkan banyaknya tiket: "))

if usia >= 15:
    print('\nAnda sudah cukup dewasa untuk naik wahana ini')
    if usia <= 20 or usia >= 60:
        print('Anda memperoleh potongan harga 20%')
    else:
        print('Maaf anda sudah dewasa, tidak mendapat potongan harga')
    if qty >= 4:
        print('Banyaknya tiket lebih dari 4, dapat bonus menarik')
    else:
        print('Banyaknya tiket kurang dari 4, tidak dapat bonus')
else:
    print('\nMaaf usia anda kurang dari 15, belum diperkenankan naik')

print("\nProgram selesai")
