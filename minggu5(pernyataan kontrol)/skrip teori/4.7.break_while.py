'''         Contoh skrip program efek break pada pernyataan while        '''

i = 1

while i < 10:
    print("i =", i, "--> Selamat Belajar Python")
    if i == 5:
        break
    i += 1