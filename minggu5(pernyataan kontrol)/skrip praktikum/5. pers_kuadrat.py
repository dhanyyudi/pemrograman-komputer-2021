# input
a = int(input("Masukkan parameter a : "))
b = int(input("Masukkan parameter b : "))
c = int(input("Masukkan parameter c : "))

# perhitungan deskriminan
desk = b*b-4*a*c
if desk > 0:
    deskriminan = desk**(1/2)
    x1 = -b+desk/2*a
    x2 = -b-desk/2*a

# uji deskriminan
if deskriminan > 0:
    print("Ada dua akar berlainan yaitu, x1", x1, "dan x2", x2)
elif deskriminan == 0:
    print("Ada satu akar kembar", x1)
else:
    print("Akar imajiner")
