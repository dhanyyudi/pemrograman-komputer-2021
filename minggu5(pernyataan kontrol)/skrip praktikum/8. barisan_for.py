'Menampilkan deretan bilangan dengan for '

baris = 10
for i in range(1, baris):
    for j in range(1, i + 1):
        print('{:<3}'.format(i * j), end = ' ')
    print()
    i += 1