'Skrip program pengujian if dan pengulangan while'
'disimpan dengan nama file uji_ulang.py'

i = int(input('Masukkan sebuah bilangan bulat = '))
# Gunakan perintah if...elif...
if i * 10 > 500 :
    y = i % 5
    print(y)
elif i / 2 < 10 :
    y = i // 5
    print(y)

# Tuliskan perintah line 6-11 dalam satu baris (if ekspresi)
print('y', i % 5 if i* 10 > 500 else i // 5)

z = 2
while z <= 20:
    a = 2 * z
    b = z * a
    z = z + 3
else:
    print(i, y, a, b)
# Tampilkan hasilnya di layar monitor
print("Untuk nilai i =", i)
print('Untuk nilai y =', y)
print('Untuk nilai a =', a)
print('Untuk nilai b =', b)