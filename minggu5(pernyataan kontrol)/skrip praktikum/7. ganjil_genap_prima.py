# input
bil = int(input("Masukkan bilangan bulat : "))

# uji bilangan
if bil % 2 == 0 and bil % 1 == 0:
    print("Bilangan yang anda masukkan :", bil, ", merupakan bilangan genap sekaligus bilangan prima ")
elif bil % 2 == 1 and bil % 1 == 0:
    print("Bilangan yang anda masukkan :", bil, ", merupakan bilangan ganjil sekaligus bilangan prima ")
elif bil % 2 == 0 and bil % 1 != 0:
    print("Bilangan yang anda masukkan :", bil, ", merupakan bilangan genap tetapi bukan bilangan prima ")
elif bil % 2 == 1 and bil % 1 != 0:
    print("Bilangan yang anda masukkan :", bil, ", merupakan bilangan ganjil tetapi bukan bilangan prima ")