tinggi = 9
i = 1

while i <= tinggi:
    kolom = 1
    while (kolom <= i):
        print('{:<3}'.format(i * kolom), end = ' ')
        kolom += 1
    print()
    i += 1